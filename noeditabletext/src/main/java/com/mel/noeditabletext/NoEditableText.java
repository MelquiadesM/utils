package com.mel.noeditabletext;


import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

public class NoEditableText extends ConstraintLayout {
    private AppCompatTextView tvHintText;
    private AppCompatTextView tvText;
    private String textHint;
    private String textHintColor;
    private int textHintSize;
    private String text;
    private int textSize;
    private String textColor;
    private int paddingBottom;
    private int paddingLeft;
    private int paddingRight;
    private Drawable drawableEnd;
    private String errorColor;
    private View lineView;
    private int lineHeigth;
    private boolean clearHintTemp;
    private boolean clearHint;


    public NoEditableText(Context context) {
        super(context);
    }

    public NoEditableText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public NoEditableText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setError() {
        if (tvText != null)
            tvText.setTextColor(errorColor.isEmpty() ? getResources().getColor(R.color.defaulErrorColor) : Color.parseColor(errorColor));
        if (tvHintText != null)
            tvHintText.setTextColor(errorColor.isEmpty() ? getResources().getColor(R.color.defaulErrorColor) : Color.parseColor(errorColor));
        if (lineView != null)
            lineView.setBackgroundColor(errorColor.isEmpty() ? getResources().getColor(R.color.defaulErrorColor) : Color.parseColor(errorColor));

    }

    public void disableError() {
        if (tvText != null)
            tvText.setTextColor(textColor.isEmpty() ? getResources().getColor(R.color.defaultColor) : Color.parseColor(textColor));
        if (tvHintText != null)
            tvHintText.setTextColor(textHintColor.isEmpty() ? getResources().getColor(R.color.defaultHintColor) : Color.parseColor(textHintColor));
        if (lineView != null)
            lineView.setBackgroundColor(textHintColor.isEmpty() ? getResources().getColor(R.color.defaultHintColor) : Color.parseColor(textHintColor));

    }

    private void init(Context context, AttributeSet attrs) {
        if (null == attrs) {
            throw new IllegalArgumentException("Sin atributos definidos");
        }
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.NoEditableText);
        textHint = typedArray.getString(R.styleable.NoEditableText_hintText);
        textHintColor = typedArray.getString(R.styleable.NoEditableText_hintTextColor);
        textHintSize = typedArray.getDimensionPixelSize(R.styleable.NoEditableText_hintTextSize, 12);
        text = typedArray.getString(R.styleable.NoEditableText_text);
        textColor = typedArray.getString(R.styleable.NoEditableText_textColor);
        textSize = typedArray.getDimensionPixelSize(R.styleable.NoEditableText_textSize, 12);
        drawableEnd = typedArray.getDrawable(R.styleable.NoEditableText_drawableEnd);
        paddingLeft = typedArray.getInt(R.styleable.NoEditableText_paddingLeft, 0);
        paddingBottom = typedArray.getInt(R.styleable.NoEditableText_paddingBottom, 0);
        paddingRight = typedArray.getInt(R.styleable.NoEditableText_paddingRight, 0);
        errorColor = typedArray.getString(R.styleable.NoEditableText_errorColor);
        lineHeigth = typedArray.getDimensionPixelSize(R.styleable.NoEditableText_lineHeigth, 5);
        clearHint = typedArray.getBoolean(R.styleable.NoEditableText_clearHint, false);
        typedArray.recycle();

        textHint = textHint == null ? "" : textHint;
        text = text == null ? "" : text;
        textHintColor = textHintColor == null ? "" : textHintColor;
        textColor = textColor == null ? "" : textColor;
        errorColor = errorColor == null ? "" : errorColor;

        actualizarVista();
        setBackground(getResources().getDrawable(R.drawable.ripple_constraint));
    }

    private void actualizarVista() {
        removeAllViews();

        if (!text.isEmpty() && !textHint.isEmpty()) {
            configurarTextViews(true);
            configurarConstraints(true);
        }

        if (text.isEmpty()) {
            clearHintTemp = false;
            configurarTextViews(false);
            configurarConstraints(false);
        }

    }

    private void configurarConstraints(boolean textVisible) {
        ConstraintSet constraintSet = new ConstraintSet();
        constraintSet.clone(this);

        constraintSet.connect(lineView.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, paddingLeft);
        constraintSet.connect(lineView.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, paddingRight);
        constraintSet.connect(lineView.getId(), ConstraintSet.TOP, tvHintText.getId(), ConstraintSet.BOTTOM, paddingBottom);
        constraintSet.constrainWidth(lineView.getId(), ConstraintSet.MATCH_CONSTRAINT);

        constraintSet.connect(tvHintText.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, paddingLeft);
        constraintSet.connect(tvHintText.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, paddingRight);
        constraintSet.connect(tvHintText.getId(), ConstraintSet.TOP, ConstraintSet.PARENT_ID, ConstraintSet.TOP);
        constraintSet.connect(tvHintText.getId(), ConstraintSet.BOTTOM, lineView.getId(), ConstraintSet.BOTTOM, paddingBottom);
        constraintSet.constrainWidth(tvHintText.getId(), ConstraintSet.MATCH_CONSTRAINT);
        if (textVisible) {
            constraintSet.connect(tvText.getId(), ConstraintSet.START, ConstraintSet.PARENT_ID, ConstraintSet.START, paddingLeft);
            constraintSet.connect(tvText.getId(), ConstraintSet.END, ConstraintSet.PARENT_ID, ConstraintSet.END, paddingRight);
            constraintSet.connect(tvText.getId(), ConstraintSet.TOP, tvHintText.getId(), ConstraintSet.BOTTOM, paddingBottom);
            constraintSet.connect(tvText.getId(), ConstraintSet.BOTTOM, lineView.getId(), ConstraintSet.BOTTOM, paddingBottom);
            constraintSet.clear(tvHintText.getId(), ConstraintSet.BOTTOM);
            constraintSet.clear(lineView.getId(), ConstraintSet.TOP);
            constraintSet.connect(lineView.getId(), ConstraintSet.TOP, tvText.getId(), ConstraintSet.BOTTOM, paddingBottom);
            constraintSet.constrainWidth(tvText.getId(), ConstraintSet.MATCH_CONSTRAINT);
            AutoTransition transition = new AutoTransition();
            transition.setDuration(200);
            transition.setInterpolator(new AccelerateDecelerateInterpolator());
            TransitionManager.beginDelayedTransition(this, transition);
        }

        constraintSet.applyTo(this);
    }

    private void configurarTextViews(boolean textVisible) {

        int i = 1000;
        tvHintText = new AppCompatTextView(getContext());
        tvHintText.setText(textHint);
        tvHintText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textHintSize);
        tvHintText.setGravity(Gravity.START);
        tvHintText.setTextColor(textHintColor.isEmpty() ? getResources().getColor(R.color.defaultHintColor) : Color.parseColor(textHintColor));
        tvHintText.setTypeface(null, Typeface.NORMAL);


        if (!textVisible) {
            tvHintText.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableEnd, null);
        }

        tvHintText.setId(i + 2000);
        addView(tvHintText);

        if (clearHintTemp) {
            tvHintText.setVisibility(GONE);
        }

        if (textVisible) {
            tvText = new AppCompatTextView(getContext());
            tvText.setText(text);
            tvText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvText.setGravity(Gravity.START);
            tvText.setTextColor(textColor.isEmpty() ? getResources().getColor(R.color.defaultColor) : Color.parseColor(textColor));
            tvText.setTypeface(null, Typeface.NORMAL);
            tvText.setId(i + 1000);
            tvText.setCompoundDrawablesWithIntrinsicBounds(null, null, drawableEnd, null);
            addView(tvText);
        }

        lineView = new View(getContext());
        lineView.setId(i + 3000);
        lineView.setBackgroundColor(textHintColor.isEmpty() ? getResources().getColor(R.color.defaultHintColor) : Color.parseColor(textHintColor));
        lineView.setLayoutParams(new ConstraintLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        lineView.getLayoutParams().height = lineHeigth;
        addView(lineView);
    }

    public void setText(String text) {
        this.text = text;

        if (text != null && !text.trim().isEmpty()) {
            clearHintTemp = clearHint;
        } else {
            clearHintTemp = false;
        }

        actualizarVista();
    }

    public void setHintText(String text) {
        this.textHint = text;
        actualizarVista();
    }

    public String getText() {
        return text != null ? text : "";
    }

}
