package com.mel.utils;

import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;
import java.util.Collections;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

public class MainActivity extends AppCompatActivity {
    private ViewPager vpExample;
    private CustomTabLayout tabExample;
    private ArrayList<Integer> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        vpExample = findViewById(R.id.vpExample);
        tabExample = findViewById(R.id.tExample);

        vpExample.setAdapter(new ViewPagerAdapter());
        tabExample.setupWithViewPager(vpExample);
        ArrayList<String> titles = new ArrayList<>();
        titles.add("Titulo 1");
        titles.add("Titulo 2");
        titles.add("Titulo 3");
        titles.add("Titulo 4");
        items = new ArrayList<>();
        items.add(0);
        items.add(1);
        items.add(2);
        items.add(3);
        tabExample.setTitles(titles);
    }

    public void setError(View view) {
        Collections.shuffle(items);

        tabExample.setTabError(items.get(0));
    }

    public void clearErrors(View view) {
        tabExample.clearAllErrors();
    }
}
